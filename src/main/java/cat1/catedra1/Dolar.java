/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat1.catedra1;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
/**
 *
 * @author JOSEANTONIOJAROA
 */
@Path("/")
public class Dolar {
    @Context
    private HttpHeaders header;
    @Context
    HttpServletRequest request;
    
    @GET
    @Path("dolar")
    @Produces({MediaType.TEXT_HTML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getCambioDolar(@QueryParam("fecha") String fecha) throws MalformedURLException, ProtocolException, IOException, ParseException{
   
     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = sdf.parse(fecha);
                System.out.println(date.toString());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
	//	calendar.add(Calendar.MONTH, 1);
		System.out.println("dia="+calendar.get(Calendar.DAY_OF_MONTH));
		System.out.println("año="+calendar.get(Calendar.YEAR));
		System.out.println("mes="+calendar.get(Calendar.MONTH));
                int mesajuste= calendar.get(Calendar.MONTH)+1;                

                
                
                
                System.out.println("El dia de la semana es "+calendar.get(Calendar.DAY_OF_WEEK));
                System.out.println(calendar);
                int diaSemana=calendar.get(Calendar.DAY_OF_WEEK);
                    if (diaSemana == 7) {
                        System.out.println("FIN DE SEMANA");
                         calendar.add(Calendar.DATE, -1);
                         System.out.println("Nuevo dia="+calendar.get(Calendar.DAY_OF_MONTH));
                   
                     } else if (diaSemana == 1) {
                        calendar.add(Calendar.DATE, -2);
                        System.out.println("fin de semana");
                        System.out.println("Nuevo dia="+calendar.get(Calendar.DAY_OF_MONTH));
                       
                    } 
                
               
                int dia=calendar.get(Calendar.DAY_OF_MONTH);
		int anio=calendar.get(Calendar.YEAR);

                  String URL = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"+anio+"/"+mesajuste+"/dias/"+dia+"?apikey=75c383cd785199bab4bdc2b24c91220eb8cca4cd&formato=JSON";
           
 
       
        String respuesta = "";
        String urlGET = URL;
        
             try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(urlGET);
            get.setHeader("Content-type", "application/json");
            HttpResponse responseST = httpClient.execute(get);

            HttpEntity entityResult = responseST.getEntity();
               System.out.println("la fecha es "+fecha+" Y la respuesta "+entityResult);
               
               
         if (entityResult!= null) {
                InputStream instream = entityResult.getContent();
                String resultadoRequest = convertStreamToString(instream);
                instream.close();
                respuesta = resultadoRequest;
            } else {
                respuesta = "error";
            }        
         
         
        } catch (IOException | UnsupportedOperationException ex) {
            System.out.println("respuesta " + ex);
            respuesta = "error";
        }
        String valor=respuesta.substring(respuesta.indexOf("Valor")+9,respuesta.indexOf("Valor")+15);
        System.out.println("y la respuesta es "+respuesta);
        return Response.ok("<!DOCTYPE html>\n" +
"<html>\n" +
"\n" +
"    <head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
"        <title>JSP Page Valor dolar</title>\n" +
"        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n" +
"\n" +
"    </head>\n" +
"    <body>\n" +
"        <style type=\"text/css\">\n" +
"      a,h1,h5,h6,p {color:white}\n" +
"     form{color:white }\n" +
"      body{background: #34e89e;  /* fallback for old browsers */\n" +
"background: -webkit-linear-gradient(to right, #0f3443, #34e89e);  /* Chrome 10-25, Safari 5.1-6 */\n" +
"background: linear-gradient(to right, #0f3443, #34e89e); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n" +
"}\n" +
"    </style>\n" +
"        <div class= \"container \">\n" +
"        <h1>Valor del Dolar en Pesos Chilenos </h1>\n" +
"        <p>Catedra numero 1 ACI810<p>\n" +
"        <p>Estudiantes: Alvaro Fuenzalida y José Antonio Roa <p>\n" +
"        <h5>Profesor David Enamorado</h5>\n" +
"        \n" +
"        <h1>El valor del del dolar norteamericano el día "+dia+"/"+mesajuste+"/"+anio +" es "+valor+
"   \n</h1>" +
"        \n" +
"        \n" +
        " <p>Informacion obtenida de sbif.cl <p>\n" +
"        \n" +
"        <p> \n" +
"           \n" +
"        </p>\n" +
"        \n" +
"        </div>\n" +
"        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n" +
"<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n" +
"<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n" +
"    </body>\n" +
"</html>").build();        
    }   

public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 System.out.println("Dentro del ConvertStrem" );
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            System.out.println("readline " + e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                System.out.println("close " + e);
            }

        }
System.out.println("Dentro del Convert mostrando fecha: " );
        return sb.toString();

    }

}