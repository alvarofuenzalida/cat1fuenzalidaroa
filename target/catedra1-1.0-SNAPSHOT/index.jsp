<%-- 
    Document   : dollar
    Created on : Sep 28, 2019, 5:20:15 PM
    Author     : Alvaro F. JOSEANTONIOJAROA 
--%>

<%@page import="javax.ws.rs.core.Response"%>
<%@page import="javax.ws.rs.client.ClientBuilder"%>
<%@page import="javax.ws.rs.client.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page Valor dolar</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    </head>
    <body>
        <style type="text/css">
      a,h1,h5,h6,p {color:white}
     form{color:white }
      body{background: #34e89e;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #0f3443, #34e89e);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #0f3443, #34e89e); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
}
    </style>
        <div class= "container ">
        <h1>Valor del Dolar en Pesos Chilenos </h1>
        <p>Catedra numero 1 ACI810<p>
        <p>Estudiantes: Alvaro Omar Fuenzalida Muñoz y José Antonio Roa Canales <p>
        <h5>Profesor David Enamorado</h5>
        
        <form class="card p-2" action="/api/dolar">
              
              
                 Fecha:<br>
               
              <input type="date"  placeholder="dd-MM-yyyy" class="form-control" name="fecha" required autocomplete="off" >  
              <input type="Submit" value="Consultar">
              
        </form>
        <h6>Información financiera obtenida desde https://sbif.cl/</h6>
   
        
        
        
        <p> 
           
        </p>
        
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
